//
// xmega 8e5 interrupt example
//
// with a button on pin 4 port C and an LED on pin 0 port C
#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>

#define SLAVE_ADDRESS    0x56
#define CPU_SPEED       32000000

// ISR = interrupt service routine, program that runs when an interrupt is triggered on PORTC
ISR(PORTC_INT_vect)
{
    // Toggle LED on interrupt
    PORTC.OUTSET = PIN0_bm;
    _delay_ms(100);
    PORTC.OUTCLR = PIN0_bm;

    // the interrupt has to be cleared within the ISR
    PORTC.INTFLAGS = PIN4_bm;
}


int main(void) {
  // set up clock
  OSC.CTRL = OSC_RC32MEN_bm; // enable 32MHz clock
  while (!(OSC.STATUS & OSC_RC32MRDY_bm)); // wait for clock to be ready
  CCP = CCP_IOREG_gc; // enable protected register change
  CLK.CTRL = CLK_SCLKSEL_RC32M_gc; // switch to 32MHz clock
  
  //enable interrupts
  PMIC.CTRL |= PMIC_LOLVLEX_bm;
  
  //set up LED as output 
  PORTC.DIRSET = PIN0_bm;
  
  // External interrupt on PC4, enable internal pullup resistor, sense falling edge
  PORTC.PIN4CTRL = PORT_OPC_PULLUP_gc | PORT_ISC_FALLING_gc;
  PORTC.INTMASK = PIN4_bm;
  PORTC.INTCTRL = PORT_INTLVL_LO_gc;
  // Enable low level interrupts
  PMIC.CTRL |= PMIC_LOLVLEN_bm;
  sei(); //set global interrupt enable

  while(1){
   }
}










