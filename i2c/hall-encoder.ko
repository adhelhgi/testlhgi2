from koko.lib.pcb import *
from koko.lib.sam import *

class TagConnectPDI(Component):
    '''
    '''
    _pad = s2d.circle(0,0,.5*.031)
    _via = s2d.circle(0,0,.5*.039)
    pins = [
        Pin(-.05,-.025,_pad,'CLK',label_size=.02),
        Pin( .00,-.025,_pad,'NC',label_size=.02),
        Pin( .05,-.025,_pad,'DAT',label_size=.02),
        Pin(-.05,.025,_pad,'VCC',label_size=.02),
        Pin( .00,.025,_pad,'NC',label_size=.02),
        Pin( .05,.025,_pad,'GND',label_size=.02)
    ]
    vias = [
        Via(-.1,0,_via),
        Via(.1,0.04,_via),
        Via(.1,-.04,_via),

    ]

class ATxmega16A4U(Component):
    _padh = chamfered_rectangle(-.024,.024,-.007,.007,.003)
    _padv = chamfered_rectangle(-.007,.007,-.024,.024,.003)
    c= 11/25.4/2.
    d = 0.8/25.4
    names = [
        'PA5','PA6','PA7','PB0','PB1','PB2','PB3','GND','VCC','PC0','PC1',
        'PC2','PC3','PC4','PC5','PC6','PC7','GND2','VCC2','PD0','PD1','PD2',
        'PD3','PD4','PD5','PD6','PD7','PE0','PE1','GND3','VCC3','PE2','PE3',
        'PDI/DATA','RST/CLK','PR0','PR1','GND4','AVCC','PA0','PA1','PA2','PA3','PA4'
    ]
    n_side = 11
    pins = [Pin(-c,(5-i)*d,_padh,n,label_size=.025) for i,n in enumerate(names[:n_side])]
    pins += [Pin((-5+i)*d,-c,_padv,n,label_size=.025,label_rot=-90) for i,n in enumerate(names[n_side:2*n_side])]
    pins += [Pin(c,(-5+i)*d,_padh,n,label_size=.025) for i,n in enumerate(names[2*n_side:3*n_side])]
    pins += [Pin((5-i)*d,c,_padv,n,label_size=.025,label_rot=-90) for i,n in enumerate(names[3*n_side:])]
    vias = []
    shadow = rectangle(-c+d,c-d,-c+d,c-d)

class ATxmegaE5(Component):
    _padh = chamfered_rectangle(-.025,.025,-.007,.007,.002)
    _padv = chamfered_rectangle(-.007,.007,-.025,.025,.002)
    c=.18
    d = 0.8/25.4
    names = [
        '1 GND','PA4','PA3','PA2','PA1','PA0','PDI/DATA','RST/CLK',
        'PC7','PC6','PC5','PC4','PC3','PC2','PC1','PC0',
        'VCC','GND','PR1','PR0','PD7','PD6','PD5','PD4',
        'PD3','PD2','PD1','PD0','PA7','PA6','PA5','AVCC'
    ]
    pins = [Pin(-c,(3.5-i)*d,_padh,n,label_size=.025) for i,n in enumerate(names[:8])]
    pins += [Pin((-3.5+i)*d,-c,_padv,n,label_size=.025,label_rot=-90) for i,n in enumerate(names[8:16])]
    pins += [Pin(c,(-3.5+i)*d,_padh,n,label_size=.025) for i,n in enumerate(names[16:24])]
    pins += [Pin((3.5-i)*d,c,_padv,n,label_size=.025,label_rot=-90) for i,n in enumerate(names[24:])]
    vias = []
    shadow = rectangle(-c+d,c-d,-c+d,c-d)

class Regulator_SOT23(Component):
    ''' LM3480 100-mA, 3.3V
    '''
    _pad_SOT23 = s2d.rectangle(-.02,.02,-.012,.012)
    pins = [
        Pin(-0.045, -0.0375, _pad_SOT23,'IN'),
        Pin(-0.045,  0.0375, _pad_SOT23,'OUT'),
        Pin(0.045, 0, _pad_SOT23,'GND')
    ]
    prefix = 'U'
    vias = []


class Header_FTDI(Component):
    ''' For ftdi cable
    '''
    _pad_header  = chamfered_rectangle(-0.06, 0.06, -0.025, 0.025,.007)
    pins = [
        #Pin(0,  -0.25, _pad_header, 'RTS'),
        Pin(0,  -0.15, _pad_header, 'RX'),
        Pin(0,  -0.05, _pad_header, 'TX'),
        Pin(0,   0.05, _pad_header, 'VCC'),
        Pin(0,   0.15, _pad_header, 'CTS'),
        Pin(0,   0.25, _pad_header, 'GND'),
    ]
    prefix = 'J'
    vias = []
    shadow = s2d.rectangle(-.06,8/25.4,-.18,.28)

class AS5510(Component):
    #Austrian Microsystems AS5510 linear magnetic encoder
    _pad_SOIC = s2d.rectangle(-0.041, 0.041, -0.015, 0.015)
    pins = []
    y = 0.075
    for t in ['NC', 'VSS', 'ADR', 'VDD']:
        pins.append(Pin(-0.14, y, _pad_SOIC, t))
        y -= 0.05
    for p in ['TEST', 'SDA', 'SCL', 'NC']:
        y += 0.05
        pins.append(Pin(0.14, y, _pad_SOIC, p))
    del y
    prefix = 'U'
    w = 5/25.4#3.90/25.4; 
    h = 4.9/25.4
    vias = []
class AS5510_Flipped(Component):
    #Austrian Microsystems AS5510 linear magnetic encoder
    _pad_SOIC = s2d.rectangle(-0.041, 0.041, -0.015, 0.015)
    pins = []
    y = 0.075
    for t in ['NC', 'VSS', 'ADR', 'VDD']:
        pins.append(Pin(0.14, y, _pad_SOIC, t))
        y -= 0.05
    for p in ['TEST', 'SDA', 'SCL', 'NC']:
        y += 0.05
        pins.append(Pin(-0.14, y, _pad_SOIC, p))
    del y
    prefix = 'U'
    w = 5/25.4#3.90/25.4; 
    h = 4.9/25.4
    vias = [
        Via(0,0,dogboned_rectangle_y(-.5*w,.5*w,-.5*h,.5*h,.016)) ]
class Hole(Component):
    pins = [Pin(0,0,circle(0,0,0.01))]
    vias = [Via(0,0,circle(0,0,.5*2.1/25.4))]
    tap = circle(0,0,.5*1.9/25.5)


width = 1.5
height = 1.
pcb = PCB(0,0,width,height,chamfer_distance=.12)

def connectG(pin,dx,dy,width=.014):
    '''
    Convenience function for connecting to ground plane
    '''
    pcb.connectD(pin,[pin.x+dx,pin.y+dy],[pin.x+dx-.0001,pin.y+dy],width=width,sides=[0,1,1])
def connectS(pin,dx,dy,width=.014):
    pcb.connectD(pin,[pin.x+dx+.0001,pin.y+dy],width=width)
def connectM(pin1,pin2,dx,width=.014):
    pcb.connectD(pin1,[pin1.x+dx,pin1.y],pin2,width=width)

#pcb.custom_cutout = chamfered_rectangle(0,width,.5*height-.25,.5*height+.25,.05)
#pcb.custom_cutout += chamfered_rectangle(0,.6*width,0,height,.12)


xmega = ATxmegaE5(.45,.7,90-45,'Xmega\n8E5')
pcb += xmega

pdi = TagConnectPDI(xmega.x+.04,xmega.y-.36,180)
pcb += pdi


pcb.connectD(xmega['RST/CLK'],[pdi['CLK'].x,pdi['CLK'].y+.05],pdi['CLK'],width=.014)
#connectG(xmega['1 GND'],-.03,.05)
#connectG(xmega['GND'],.07,.05)

pcb.connectD(xmega['PDI/DATA'],[xmega['PDI/DATA'].x-.03,xmega['PDI/DATA'].y-.04],pdi['DAT'],width=.014)

C1 = C_0805(pdi.x,pdi.y-.08,180,'C1 1uF')
pcb += C1
C2 = C_0805(C1.x,C1.y-.07,180,'C2 .1uF')
pcb += C2
R1 = R_0805(pdi.x+.11,pdi.y+.085,0,'R1\n10k')
pcb += R1

pcb.connectD(xmega['1 GND'],[xmega['1 GND'].x-.03,xmega['1 GND'].y-.04],[C2[0].x-.14,C2[0].y+.1],C2[0],width=.014)


pcb.connectV(pdi['GND'],C1[0])
pcb.connectV(pdi['GND'],C2[0])
pcb.connectV(pdi['VCC'],C1[1])
pcb.connectV(pdi['VCC'],C2[1])

pcb.connectD(xmega['RST/CLK'],R1[1],width=.014)
pcb.connectD(xmega['AVCC'],[xmega['AVCC'].x+.188,xmega['AVCC'].y-.1],xmega['PC5'],R1[0],width=.014)

pcb.connectD(xmega['VCC'],[xmega.x-.105,xmega.y-.05],xmega['AVCC'],width=.014)


ftdi = Header_FTDI(xmega.x-.28,xmega.y-.35,180,'ftdi')
pcb += ftdi
reg = Regulator_SOT23(ftdi.x+.15,ftdi.y-.25,180,'3.3v')
pcb += reg
pcb.connectD(ftdi['VCC'],[ftdi.x+.05,ftdi['VCC'].y],reg['IN'],width=.02)
pcb.connectV(R1[0],C1[1],width=.018)

pcb.connectD(xmega['PD2'],[xmega['PD2'].x-.05,xmega['PD2'].y+.035],[xmega.x-.3,xmega.y-.07],[ftdi.x+.1,ftdi['TX'].y],ftdi['TX'],width=.014)
pcb.connectD(xmega['PD3'],[xmega['PD3'].x-.08,xmega['PD3'].y+.05],[xmega.x-.35,xmega.y-.1],ftdi['RX'],width=.014)


pcb.connectV(C2[0],ftdi['GND'])
pcb.connectV(reg['GND'],ftdi['GND'])
pcb.connectV(C1[1],reg['OUT'],width=.02)

pcb.connectD(xmega['GND'],[xmega.x,xmega['GND'].y-.03],[xmega.x,xmega.y+.2],[ftdi.x+.13,xmega.y+.29],[ftdi['GND'].x-.1,ftdi['GND'].y+.05],ftdi['GND'],width=.014)


hall = AS5510_Flipped(width-.3,height*.5,0,'AS5510')
pcb += hall

Chall = C_1206(hall.x+.24,hall['ADR'].y,-90,'100 nF')
pcb += Chall
pcb.connectV(hall['ADR'],hall['VSS']) #ground ADR to pick 1010110 as I2C address
pcb.connectH(hall['VSS'],Chall[1])
pcb.connectH(hall['VDD'],Chall[0])

#pcb.connectV(hall['TEST'],[Chall.x+.07,Chall.y-.12],Chall[1])
pcb.connectD(xmega['GND'],[xmega['GND'].x+.04,xmega['GND'].y+.03],[Chall.x-.05,hall.y+.15],Chall[1],width=.014)

pcb.connectD(xmega['PC0'],hall['SDA'],width=.014)
pcb.connectD(xmega['PC1'],[xmega['PC1'].x+.1,xmega['PC1'].y-.1],[xmega['PC1'].x+.15,xmega['PC1'].y-.05],hall['SCL'],width=.014).add_jumper([xmega['PC1'].x+.12,xmega['PC1'].y-.075],45)



RSCL = R_1206(R1.x+.12,R1.y-.05,90,'RSCL',label_size=.03)
pcb += RSCL
RSDA = R_1206(RSCL.x+.1,RSCL.y,90,'RSDA',label_size=.03)
pcb += RSDA

pcb.connectV(hall['VDD'],RSDA[1]).cut_corners([(1,.08)])
pcb.connectD(hall['TEST'],[RSCL.x+.25,hall['TEST'].y],[RSCL.x+.22,ftdi['GND'].y+.0],[ftdi['GND'].x+.05,ftdi['GND'].y-.08],ftdi['GND']).add_jumper([RSCL.x+.22,RSDA[1].y],90)

pcb.connectH(C1[1],RSCL[1])
pcb.connectH(C1[1],RSDA[1])
pcb.connectV(RSDA[0],hall['SDA'])
pcb.connectD(xmega['PC1'],RSCL[0],width=.014)


#soldering tails
connectS(xmega['PA7'],.04,-.04)
connectS(xmega['PA6'],.04,-.04)
connectS(xmega['PA5'],.04,-.04)
connectS(xmega['PD0'],.04,-.04)
connectS(xmega['PD1'],.04,-.04)
connectS(xmega['PA4'],.03,.03)
connectS(xmega['PA3'],.03,.03)
connectS(xmega['PA2'],.03,.03)
connectS(xmega['PA1'],.03,.03)
connectS(xmega['PA0'],.03,.03)

connectS(xmega['PD4'],.04,.04)
connectS(xmega['PD5'],.04,.04)
connectS(xmega['PD6'],.04,.04)
connectS(xmega['PD7'],.04,.04)
connectS(xmega['PR0'],.04,.04)
connectS(xmega['PR1'],.04,.04)

connectS(xmega['PC2'],.04,-.04)
connectS(xmega['PC3'],.04,-.04)
connectS(xmega['PC4'],.04,-.04)
connectS(xmega['PC6'],.03,-.03)
connectS(xmega['PC7'],.02,-.02)



wjw = 4
wjh = 1.2
fl = 1.25 #flexure length
ratio = 12.8 #length over flexure width
fth = fl/ratio#.06
fl_gap = .05
fw = wjh-2*fth-fl_gap

wj = chamfered_rectangle(-wjw+width,width,.5*height-.5*wjh,.5*height+.5*wjh,.1)
wj -= circle(hall.x,hall.y,4/25.4)

wj -= make_edges([
    [hall.x-.25-fl,hall.y-.5*fw],
    [hall.x-.25,hall.y-.5*fw],
    [hall.x-.25,hall.y+.5*fw],
    [hall.x-.25-fl,hall.y+.5*fw]
    ],fl_gap)

mount_pts = [[hall.x-.4,hall.y+.35],[hall.x-.4,hall.y-.35]]

for p in mount_pts:
    h = Hole(p[0],p[1],0)
    wj -= move(h.tap,p[0],p[1])
    pcb += h

#wj -= pcb.cutout
#pcb.add_custom_layer("wj",wj,(250,0,100))

cad.shapes = pcb.layout
#cad.shape = pcb.traces+(pcb.cutout-pcb.cutout)
#cad.shape = pcb.cutout+(pcb.traces-pcb.traces)
#cad.shape = pcb.custom_layers['wj']['layer']