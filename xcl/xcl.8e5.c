//
// xcl.8E5.c
//
// xmeg 8E5 custom logic block
//

#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>


enum xcl_lut_thruth_t
                   {
    /* ! AND logic function */
    AND    = 0x8,
    /* ! NAND logic function */
    NAND   = 0x7,
    /* ! OR logic function */
    OR     = 0xE,
    /* ! XOR logic function */
    XOR    = 0x6,
    /* ! NOR logic function */
    NOR    = 0x1,
    /* ! XNOR logic function */
    XNOR   = 0x9,
    /* ! NOT logic function */
    NOT    = 0x3,
    /* ! Copy IN1 input */
    IN1    = 0xC,
    /* ! Copy IN3 input */
    IN3    = 0xC,
    /* ! Copy IN2 input */
    IN2    = 0xA,
    /* ! Copy IN0 input */
    IN0    = 0xA,
};
enum     xcl_lut_in_src_t { 
  LUT_IN_EVSYS = 0, 
  LUT_IN_XCL = 0x01, 
  LUT_IN_PINL = 0x02, 
  LUT_IN_PINH = 0x03 
};

int main(void) {
   //OSC.CTRL = OSC_RC32MEN_bm; // enable 32MHz clock
   //while (!(OSC.STATUS & OSC_RC32MRDY_bm)); // wait for clock to be ready
   //CCP = CCP_IOREG_gc; // enable protected register change
   //CLK.CTRL = CLK_SCLKSEL_RC32M_gc; // switch to 32MHz clock

   OSC.CTRL = OSC_RC2MEN_bm; // enable 32MHz clock
   while (!(OSC.STATUS & OSC_RC2MRDY_bm)); // wait for clock to be ready
   CCP = CCP_IOREG_gc; // enable protected register change
   CLK.CTRL = CLK_SCLKSEL_RC2M_gc; // switch to 32MHz clock

   /*
   OSC.PLLCTRL = OSC_PLLFAC4_bm | OSC_PLLFAC3_bm; // 2 MHz * 24 = 48 MHz
   OSC.CTRL = OSC_PLLEN_bm; // enable PLL
   while (!(OSC.STATUS & OSC_PLLRDY_bm)); // wait for PLL to be ready
   CCP = CCP_IOREG_gc; // enable protected register change
   CLK.CTRL = CLK_SCLKSEL_PLL_gc; // switch to PLL
   */

   //set up xcl
   //xcl_enable(XCL_ASYNCHRONOUS);
   //sysclk_enable_module(SYSCLK_PORT_GEN, SYSCLK_XCL)
   //XCL.XXX = XCL_CLKSEL_DIV1_gc; //set clock divider
   //SYSCLK_PORT_GEN = ~SYSCLK_XCL;
   XCL.CTRLE &= ~XCL_CLKSEL_gm;
   XCL.CTRLE |= XCL_CLKSEL_DIV1_gc; //set clock divider

   //set up xcl on port D
   XCL.CTRLA &= ~XCL_PORTSEL_gm;
   XCL.CTRLA |= XCL_PORTSEL_PD_gc;

   //lut type LUT_2LUT2IN
   XCL.CTRLA &= ~XCL_LUTCONF_gm;
   XCL.CTRLA |= XCL_LUTCONF_2LUT2IN_gc;

   //set lut in 0 as LUT_IN_PINL
   XCL.CTRLB &= ~XCL_IN0SEL_gm;
   XCL.CTRLB |= LUT_IN_PINL << XCL_IN0SEL_gp; //set pd2 as in0

   //set lut in 1 as LUT_IN_PINL
   XCL.CTRLB &= ~XCL_IN1SEL_gm;
   XCL.CTRLB |= LUT_IN_PINH << XCL_IN1SEL_gp; //set pd4 as in1

   //set lut out as LUT0_OUT_PIN0
   PORTD.DIRSET = PIN0_bm;
   XCL.CTRLA &= ~XCL_LUT0OUTEN_gm;
   XCL.CTRLA |= XCL_LUTOUTEN_PIN0_gc; //set pd0 as out

   //disable delay
   XCL.CTRLC = 0;
   //XCL.CTRLC |= XCL_DLYSEL_DLY11_gc | XCL_LUTOUTEN_DISABLE_gc | XCL_LUTOUTEN_DISABLE_gc << XCL_DLY1CONF_gp;
   XCL.CTRLC |= XCL_DLYSEL_DLY22_gc | XCL_LUTOUTEN_DISABLE_gc | XCL_LUTOUTEN_DISABLE_gc << XCL_DLY1CONF_gp;

   //set lut truth function
   XCL.CTRLD &= ~XCL_TRUTH0_gm;
   XCL.CTRLD |= AND << XCL_TRUTH0_gp;

   while(1){}
}
