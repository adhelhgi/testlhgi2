### USART 

The easiest way to look at the output of USART is to use a serial monitor. The Arduino IDE and coolTerm are both freely available and provide this capability. The Arduino IDE automatically interprets the bytes sent as ASCII so you need to translate using an [ASCII table](http://www.asciitable.com/).